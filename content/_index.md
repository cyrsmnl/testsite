
# Getting Started with AWS CLI on mac OS

Install

$ curl -O https://bootstrap.pypa.io/get-pip.py
$ python3 get-pip.py --user

$ pip3 install awscli --upgrade --user

$ aws --version


Configure

`aws configure`


# Generate Key Pair on mac OS

`ssh-keygen -t rsa -b 4096 -C "name@company.com"`

copy it
`pbcopy < ~/.ssh/cysmnl.pub`

# EC2 Instance

Import Key Pair 

Retrieving the Public Key for Your Key Pair on Linux
```
ssh-keygen -y
~/.ssh/freeme.pem
```
setup permissions
`chmod 400 ~/.ssh/freeme.pem`

Setup SSH access for the desired security group. 
EC2 Dashboard > Network & Security > Security Groups

Launch a new Debian Stretch
`aws ec2 run-instances --image-id ami-0ebddb8af2428fc2e --count 1 --instance-type t2.medium --key-name freeme --security-group-ids sg-2dfbbc56`



## Explore Instances
`aws ec2 describe-instances`

`aws ec2 get-console-output --instance-id i-08df87fff0e62861e`


## Start and Stop Instances (CLI)
`aws ec2 start-instancs --instance-ids i-08df87fff0e62861e`

`aws ec2 stop-instances --instance-ids i-08df87fff0e62861e`

for detailed monitoring
`--monitoring Enabled=true` 

### Linux User Admin

`sudo adduser todd --disabled-password`

`sudo su todd`

create .ssh folder and change permissions
```
cd
mkdir .ssh
chmod 700 .ssh
```

create authorized_keys file and restrict file permissions to todd
```
cd
touch .ssh/authorized_keys
chmod 600 .ssh/authorized_keys
```

copy the public key over
`cat >> .ssh/authorized_keys`

delete user

`deluser`

Setting up user
`useradd`
`deluser`

Change user password
`passwd username`

Add user to sudo group
usermod -aG sudo username

`ssh -i [/path/to/private_key] todd@13.56.208.150`

